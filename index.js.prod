import * as React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import retargetEvents from 'react-shadow-dom-retarget-events';
import htmlToReact from 'html-to-react';

import './index.css';
import App from './App';

class ReactElement extends HTMLElement {
  
  constructor() {
    super();
    this.observer = new MutationObserver(() => this.update());
    this.observer.observe(this, { attributes: true });
  }
  mountPoint;

  connectedCallback() {
    this.mount();
  }

  disconnectedCallback() {
    this.unmount();
    this.observer.disconnect();
  }

  update() {
    this.unmount();
    this.mount();
  }

  mount() {
    if (!this.mountPoint) {
      this.mountPoint = document.createElement('div');
      this.shadow = this.attachShadow({ mode: 'open' });
      const link = document.createElement('link');
      link.setAttribute('rel', 'stylesheet');
      link.setAttribute('href', `${process.env.REACT_APP_CONTENT_SERVER}/static/css/main.css`);
      this.shadow.appendChild(link);
      //this.mountPoint.appendChild(link);
      this.shadow.appendChild(this.mountPoint);
      Object.defineProperty(this.mountPoint, "ownerDocument", { value: this.shadow });
      this.shadow.createElement = (...args) => document.createElement(...args);
    }
    const propTypes = App.propTypes ? App.propTypes : {};
    const events = App.propTypes ? App.propTypes : {};
    const props = {
      ...this.getProps(this.attributes, propTypes),
      ...this.getEvents(events),
    };
    render(<App {...props} />, this.mountPoint);
  }

  unmount() {
    unmountComponentAtNode(this.mountPoint);
  }

  parseHtmlToReact(html) {
    return html && new htmlToReact.Parser().parse(html);
  }

  getProps(attributes, propTypes) {
    propTypes = propTypes|| {};
    return [ ...attributes ]         
      .filter(attr => attr.name !== 'style')         
      .map(attr => this.convert(propTypes, attr.name, attr.value))
      .reduce((props, prop) => 
        ({ ...props, [prop.name]: prop.value }), {});
  }

  getEvents(propTypes) {
    return Object.keys(propTypes)
      .filter(key => /on([A-Z].*)/.exec(key))
      .reduce((events, ev) => ({
        ...events,
        [ev]: args => 
        this.dispatchEvent(new CustomEvent(ev, { ...args }))
      }), {});
  }

  convert(propTypes, attrName, attrValue) {
    const propName = Object.keys(propTypes)
      .find(key => key.toLowerCase() == attrName);
    let value = attrValue;
    if (attrValue === 'true' || attrValue === 'false') 
      value = attrValue == 'true';      
    else if (!isNaN(attrValue) && attrValue !== '') 
      value = +attrValue;      
    else if (/^{.*}/.exec(attrValue)) 
      value = JSON.parse(attrValue);
    return {         
      name: propName ? propName : attrName,         
      value: value      
    };
  }

}

customElements.define('react-el', ReactElement);