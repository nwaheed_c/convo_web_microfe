This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This is base project intend to be used for creating new web-components/react based micro-frontends projects.

## How to use this project

There are two parts to this. 
- One how to run it locally
- How to create production ready micro-frontend

### Common steps
- Clone this project
- Copy your work inside src directory
- Add your packages in package.json

### Running your application locally
After performing the common steps you have a separete react application. At this moment it is simple react application. There is nothing specific about web-component based micro-frontend yet. When you are done with development and verificaiton follow the process to create web-component based micro-frontend.

### How to create production ready micro-frontend
- Replace content of`index.js` in src directroy with `index.js.prod`
- Replace ReactElement class with your desired web-component name. Name must be in capital cased letter
- Open `/public/index.html` and ucomment micro-frontend relevant code. Right now it says <reat-el> here your need to specify name of your micro-frontend
- Update your env variables. It should be URL of the server where your application will be hosted
- Now run `npm run build`
- Now run `serve -s build --cors` to run the application locally

## Adding web-component into your application

Copy content on `create-wc` file and paste it where you want to include web-component on demand. Make sure to read the code, its simple and easy to use and it is only intend as an example. You can't use that code as is, you need to do evironment specific changes to use it in your application


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
